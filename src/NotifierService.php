<?php

namespace Drupal\migration_notify;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Service to trigger notifications.
 */
class NotifierService {
  use StringTranslationTrait;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\Core\Mail\MailManagerInterface definition.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $pluginManagerMail;

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new NotifierService object.
   */
  public function __construct(MessengerInterface $messenger, DateFormatterInterface $date_formatter, ModuleHandlerInterface $module_handler, StateInterface $state, MailManagerInterface $plugin_manager_mail, LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory) {
    $this->messenger = $messenger;
    $this->dateFormatter = $date_formatter;
    $this->moduleHandler = $module_handler;
    $this->state = $state;
    $this->pluginManagerMail = $plugin_manager_mail;
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
    $this->logger = $logger_factory->get('migration_notify');
  }

  /**
   * Returns when the last notification was sent.
   *
   * @return mixed
   *   Timestamp or false.
   */
  public function getLastNotificationSent() {
    return $this->state->get('migration_notify.last_sent');
  }

  /**
   * Sets the last notification time.
   *
   * @param int $time
   *   Time to set.
   */
  public function setLastNotification($time) {
    $this->state->set('migration_notify.last_sent', $time);
  }

  /**
   * Sends slack notification.
   *
   * @param string $message
   *   Message to send to slack.
   *
   * @return bool
   *   Whether the message was sent or not.
   */
  public function notifySlack($message) {
    if ($this->moduleHandler->moduleExists('slack')) {
      // We cannot inject the service as the module may not be enabled.
      // @phpstan-ignore-next-line
      $response = \Drupal::service('slack.slack_service')->sendMessage($message);
      return ($response && RedirectResponse::HTTP_OK == $response->getStatusCode());
    }

    return FALSE;
  }

  /**
   * Splits a string into an array using newlines and commas as delimiters.
   *
   * @param string $string
   *   The input string to split.
   *
   * @return array
   *   Array of trimmed non-empty strings.
   */
  public function explodeString($string) {
    // Allow commas as delimiters as well.
    $string = str_replace(',', "\n", $string);
    return array_filter(array_map('trim', explode("\n", $string)));
  }

  /**
   * Sends email notification.
   *
   * @param string $message
   *   Message to send via email.
   * @param string $emails
   *   Recipients of email if different from site administrator.
   *   Can be multiple emails separated by newlines or commas.
   *
   * @return bool
   *   Whether all messages were sent successfully.
   */
  public function notifyEmail($message, $emails = NULL) {
    $module = 'migration_notify';
    $key = 'migration_notify';
    $emails = empty($emails) ? $this->configFactory->get('system.site')->get('mail') : $emails;
    $params['message'] = $message;
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $send = TRUE;
    $success = TRUE;
    foreach ($this->explodeString($emails) as $to) {
      $result = $this->pluginManagerMail->mail($module, $key, $to, $langcode, $params, NULL, $send);
      if (!$result['result']) {
        $this->logger->error('Failed to send email notification to @email.', ['@email' => $to]);
        $success = FALSE;
      }
    }

    return $success;
  }

}
