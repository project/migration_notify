<?php

namespace Drupal\migration_notify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migration_notify\NotifierService;
use Drupal\migration_notify\HelperService;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Component\Utility\EmailValidatorInterface;

/**
 * Settings form for migration_notify.
 */
class MigrationNotifySettingsForm extends ConfigFormBase {

  /**
   * The notifier service.
   *
   * @var \Drupal\migration_notify\NotifierService
   */
  protected $notifier;

  /**
   * The helper service.
   *
   * @var \Drupal\migration_notify\HelperService
   */
  protected $helper;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Constructs the object.
   *
   * @param \Drupal\migration_notify\NotifierService $notifier
   *   The notifier service.
   * @param \Drupal\migration_notify\HelperService $helper
   *   The helper service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   */
  public function __construct(NotifierService $notifier, HelperService $helper, MessengerInterface $messenger, ModuleHandlerInterface $module_handler, DateFormatterInterface $date_formatter, EmailValidatorInterface $email_validator) {
    $this->notifier = $notifier;
    $this->helper = $helper;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->dateFormatter = $date_formatter;
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('migration_notify.notifier'),
      $container->get('migration_notify.helper'),
      $container->get('messenger'),
      $container->get('module_handler'),
      $container->get('date.formatter'),
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'migration_notify.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migration_notify_settings_form';
  }

  /**
   * Build the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('migration_notify.settings');

    $stuck = $this->helper->checkStuck();
    if ($stuck) {
      $message = $config->get('stuck_message') ?? $this->t('There are migrations stuck on the site.');
      $this->messenger->addMessage($message, 'warning');
    }
    else {
      $message = $this->t('There are no stuck migrations.');
      $this->messenger->addMessage($message, 'status');

      $this->helper->saveState([]);
    }

    $form['notification_frequency'] = [
      '#markup' => '<hr /><h3>' . $this->t('Notifications frequency') . '</h3>',
    ];

    $form['cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify on cron'),
      '#description' => $this->t('If there are stuck migrations, notify of these via cron.'),
      '#default_value' => $config->get('cron'),
    ];

    $form['daily'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Once a day only (cron)'),
      '#description' => $this->t('On cron, only send one notification per day.'),
      '#default_value' => $config->get('daily'),
    ];

    $last_sent = $this->notifier->getLastNotificationSent();
    if ($last_sent) {
      $date = $this->dateFormatter->format($last_sent);
      $form['message'] = [
        '#markup' => '<p>' . '<strong>' . $this->t('Last notification was sent:') . '</strong> ' . $date . '</p>',
      ];
    }

    $form['notification_types'] = [
      '#markup' => '<hr /><h3>' . $this->t('Notification types') . '</h3>',
    ];

    $email = $this->config('system.site')->get('mail');
    $form['email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Email notification'),
      '#description' => $this->t('Send the notification via email to the site administrator or the address specified below.'),
      '#default_value' => $config->get('email'),
    ];
    $form['email_to'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Send email to these addresses'),
      '#description' => $this->t('Send the email to the specified addresses. Comma separated list of emails or one email per line. Leave empty for %email (site administrator)', ['%email' => $email]),
      '#default_value' => $config->get('email_to'),
      '#attributes' => [
        'placeholder' => $email,
      ],
      '#states' => [
        'visible' => [
          ':checkbox[name="email"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    if ($this->moduleHandler->moduleExists('slack')) {
      $form['slack'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Slack notification'),
        '#description' => $this->t('Send the notification via slack.'),
        '#default_value' => $config->get('slack'),
      ];
    }
    else {
      $options = [
        'attributes' => [
          'target' => '_blank',
        ],
      ];
      $url = Url::fromUri('https://www.drupal.org/project/slack');
      $url->setOptions($options);
      $link = Link::fromTextAndUrl($this->t('Slack module'), $url)->toString();
      $form['slack_message'] = [
        '#markup' => '<p>' . $this->t('Install and configure the slack module to send notifications via slack.') . $link . '</p>',
      ];
      $form['slack'] = [
        '#type' => 'hidden',
        '#default_value' => FALSE,
      ];
    }

    $form['notification_messages'] = [
      '#markup' => '<hr /><h3>' . $this->t('Notification messages') . '</h3>',
    ];

    $form['stuck_message'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Stuck message'),
      '#description' => $this->t('The message to be sent when there are stuck migrations.'),
      '#default_value' => $config->get('stuck_message') ?? $this->t('There are migrations stuck on the site.'),
    ];

    $form['actions']['#type'] = 'actions';
    if ($stuck) {
      $form['actions']['notify_now'] = [
        '#type' => 'submit',
        '#value' => $this->t('Notify now'),
        '#submit' => [[$this, 'notifyNow']],
      ];
      $form['actions']['reset_now'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset migrations'),
        '#submit' => [[$this, 'resetNow']],
      ];
    }

    $form['#theme'] = 'system_config_form';
    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit handler.
   */
  public function resetNow(array &$form, FormStateInterface $form_state) {
    $config = $this->config('migration_notify.settings');
    $this->saveValues($form, $form_state);

    $stuck = $this->helper->checkStuck();
    if (!$stuck) {
      return;
    }

    $result = $this->helper->resetMigrations();
    if ($result) {
      $message = $this->t('Stuck migrations were reset.');
      $this->messenger->addMessage($message, 'status');

      if ($config->get('slack')) {
        if ($this->notifier->notifySlack($message)) {
          $this->messenger->addMessage($this->t('Slack: Message was successfully sent.'));
        }
        else {
          $this->messenger->addMessage($this->t('Slack: Message not sent. Please check log messages for details'), 'warning');
        }
      }

      if ($config->get('email')) {
        if ($this->notifier->notifyEmail($message, $config->get('email_to'))) {
          $this->messenger->addMessage($this->t('Email: Message was successfully sent.'));
        }
        else {
          $this->messenger->addMessage($this->t('Email: Message not sent. Please check log messages for details'), 'warning');
        }
      }
    }
    else {
      $message = $this->t('Stuck migrations could not be reset.');
      $this->messenger->addMessage($message, 'warning');
    }
  }

  /**
   * Submit handler.
   */
  public function notifyNow(array &$form, FormStateInterface $form_state) {
    $this->saveValues($form, $form_state);

    $stuck = $this->helper->checkStuck(TRUE);
    if (empty($stuck)) {
      $this->helper->saveState([]);
      return;
    }

    $this->helper->saveState($stuck);

    $config = $this->config('migration_notify.settings');

    $message = $config->get('stuck_message') ?? $this->t('There are migrations stuck on the site.');

    if ($config->get('slack')) {
      if ($this->notifier->notifySlack($message)) {
        $this->messenger->addMessage($this->t('Slack: Message was successfully sent.'));
      }
      else {
        $this->messenger->addMessage($this->t('Slack: Message not sent. Please check log messages for details'), 'warning');
      }
    }

    if ($config->get('email')) {
      if ($this->notifier->notifyEmail($message, $config->get('email_to'))) {
        $this->messenger->addMessage($this->t('Email: Message was successfully sent.'));
      }
      else {
        $this->messenger->addMessage($this->t('Email: Message not sent. Please check log messages for details'), 'warning');
      }
    }

    $this->notifier->setLastNotification(strtotime('now'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->saveValues($form, $form_state);
  }

  /**
   * Save form submitted values.
   */
  public function saveValues(array &$form, FormStateInterface $form_state) {
    $cron = $form_state->getValue('cron');
    $daily = $form_state->getValue('daily');
    $email = $form_state->getValue('email');
    $email_to = $form_state->getValue('email_to');
    $slack = $form_state->getValue('slack');
    $message = $form_state->getValue('stuck_message');

    $cleaned_emails = '';
    if (!empty($email_to)) {
      $email_list = $this->notifier->explodeString($email_to);
      if (!empty($email_list)) {
        $cleaned_emails = implode("\n", $email_list);
      }
    }

    $this->config('migration_notify.settings')
      ->set('cron', $cron)
      ->set('daily', $daily)
      ->set('slack', $slack)
      ->set('email', $email)
      ->set('email_to', $cleaned_emails)
      ->set('stuck_message', $message)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $emails = $form_state->getValue('email_to');
    if (!empty($emails)) {
      $email_list = $this->notifier->explodeString($emails);
      foreach ($email_list as $email) {
        if (!$this->emailValidator->isValid($email)) {
          $form_state->setErrorByName('email_to', $this->t('The email address %email is not valid.', [
            '%email' => $email,
          ]));
        }
      }
    }
  }

}
