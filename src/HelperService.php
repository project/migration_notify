<?php

namespace Drupal\migration_notify;

use Drupal\migrate\Plugin\MigrationPluginManager;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\RequirementsInterface;
use Drupal\migrate\Exception\RequirementsException;
use Drupal\Core\State\StateInterface;

/**
 * Class NotifierService.
 */
class HelperService {
  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Drupal\migrate\Plugin\MigrationPluginManager definition.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $migrations;

  /**
   * Constructs a new HelperService object.
   */
  public function __construct(MigrationPluginManager $migrations, StateInterface $state) {
    $this->migrations = $migrations;
    $this->state = $state;
  }

  /**
   * Check for migrations that can be stuck.
   *
   * @param bool $return_details
   *   Return list of migrations stuck instead of bool.
   *
   * @return bool|array
   *   Whether there are migrations stuck or not.
   */
  public function checkStuck($return_details = FALSE) {
    $migrations = $this->migrations->createInstances([]);
    $last_status = $this->state->get('migration_notify.last_status');
    $migrations_state = [];
    $return = FALSE;

    /** @var \Drupal\migrate\Plugin\Migration $migration */
    foreach ($migrations as $id => $migration) {
      $migration = $this->migrations->createInstance($id);
      if ($migration) {
        $status = $migration->getStatus();
        if ($status !== MigrationInterface::STATUS_IDLE) {
          // Assume stuck if last run and this aren't idle and the same status.
          if (!empty($last_status[$id]) && ($last_status[$id] == $status)) {
            $return = TRUE;
            $migrations_state[$id] = $status;
          }
          // Or if we don't have previous status and is not idle.
          elseif (empty($last_status[$id])) {
            $return = TRUE;
            $migrations_state[$id] = $status;
          }
        }
      }
    }

    return $return_details ? $migrations_state : $return;
  }

  /**
   * Saves the given migrations state as the last checked ones.
   *
   * @param array $migrations
   *   Current state of migrations.
   */
  public function saveState(array $migrations) {
    $this->state->set('migration_notify.last_status', $migrations);
  }

  /**
   * Reset the migrations that can be stuck.
   *
   * @return bool
   *   Whether the migrations were reset or not.
   */
  public function resetMigrations() {
    $migrations = $this->migrations->createInstances([]);
    $errors = [];

    /** @var \Drupal\migrate\Plugin\Migration $migration */
    foreach ($migrations as $id => $migration) {
      if ($migration->getSourcePlugin() instanceof RequirementsInterface) {
        try {
          $migration->getSourcePlugin()->checkRequirements();
          if (!$this->resetMigration($id)) {
            $errors[$id] = TRUE;
          }
        }
        catch (RequirementsException $e) {
          unset($migrations[$id]);
        }
      }
    }

    $this->saveState([]);
    return empty($errors);
  }

  /**
   * Reset individual migration.
   *
   * @param string $name
   *   Name of the migration to reset.
   *
   * @return bool
   *   Whether the migration was reset or not.
   */
  protected function resetMigration($name) {
    $migration = $this->migrations->createInstance($name);
    if ($migration) {
      $status = $migration->getStatus();
      if ($status !== MigrationInterface::STATUS_IDLE) {
        $migration->setStatus(MigrationInterface::STATUS_IDLE);
      }
      return TRUE;
    }

    return FALSE;
  }

}
