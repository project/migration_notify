<!--- cspell:ignore fjgarlin -->

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The module lets you trigger notifications depending on the current status of
your migrations in production. It checks if migrations are not idle and
compares them to their last state to determine whether they might be stuck
or not.

Notifications can be triggered via cron or instantly. Slack and email are
currently supported.

 * For a full description of the module visit:
   https://www.drupal.org/project/migration_notify

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/migration_notify


REQUIREMENTS
------------

This module requires the following modules.

* `migrate`, `migrate_plus`, `migrate_tools` modules need to be enabled.
* `slack` not required, but if enabled then slack notifications will be
supported.


INSTALLATION
------------

Install the Migration Notify module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.



CONFIGURATION
-------------

1. Navigate to `Administration > Extend` and enable the module.
2. Navigate to `Structure > Migrations > Notify` and tick the checkboxes
based on your needs.
3. Then click on Save configuration or Notify now.



MAINTAINERS
-----------

 * Fran Garcia-Linares (fjgarlin) - https://www.drupal.org/u/fjgarlin
